//
//  SearchableListViewController.swift
//  
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents
import UIKit

public protocol SearchableList: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    var searchController: UISearchController { get }
    var tableView: UITableView { get }
    var cellId: String { get }
    var detailStoryboardName: String? { get }
    var detailViewControllerID: String? { get }
    var viewModel: Searchable? { get set }
    
    func getNextPage()
    func setupSearchBar()
    func customiseSearchBar()
    func customiseTableView()
}

/// A Cell that can be used by the SearchableList's TableView, and visually represents a model
public protocol ListTableViewCell: UITableViewCell {
    func setupWith(model: Namable, viewModel: Searchable?)
    func setupSelectable(viewModel: Searchable?)
}

public extension ListTableViewCell {
    /// Sets the visual aspects for the View relating to if the rows should be selectable
    func setupSelectable(viewModel: Searchable?) {
        if viewModel?.selectable != true { selectionStyle = .none }
    }
}

/// A view controller for a UITableView that can show a list of Namable models, search through them by name, and allow them to be selected to display a detail view
open class SearchableListViewController<Model: Namable, Cell: ListTableViewCell>: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    // MARK: - Public fields
    /// CellID of any required ListTableViewCell to represent the models in the list
    public var cellId: String = ""
    /// Storyboard name of any required Storyboard from which to load a DetailView if the rows are selectable
    public var detailStoryboardName: String?
    /// ViewControllerID of any required DetailViewController to control a DetailView if the rows are selectable
    public var detailViewControllerID: String?
    public var viewModel: Searchable?
    
    @IBOutlet public weak var tableView: UITableView!
    
    // MARK: - Private fields
    private let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - Basic methods
    
    /// Set ViewModel in subclass before calling this super method
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupSearchBar()
        
        if viewModel?.displayList.isEmpty == true { getNextPage() }
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        customiseTableView()
        customiseSearchBar()
        customiseNavBar()
    }
    
    // TODO: Implement pagination to call getNextPage()
    public func getNextPage() {
        viewModel?.getNextPage(completion: { self.loadData() })
    }
    
    private func loadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - TableView
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.displayList.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! Cell
        if let model = viewModel?.displayList[indexPath.row] {
            cell.setupWith(model: model, viewModel: viewModel)
        }
        
        return cell
    }
    
    private func customiseTableView() {
        tableView.separatorColor = .lightGray
        tableView.separatorInset.right = tableView.separatorInset.left // Adds trailing padding to separator
    }
    
    // MARK: - Searchable
    
    public func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        
        viewModel?.searchTerm = searchText
        loadData()
    }
    
    private func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        
        definesPresentationContext = true
    }
    
    private func customiseSearchBar() {
        let searchBar = searchController.searchBar
        
        searchBar.searchTextField.leftView?.tintColor = .lightGray
        searchBar.searchTextField.textColor = .white
        searchBar.searchTextField.backgroundColor = .gray.withAlphaComponent(0.3)
        
        searchBar.searchTextField.attributedPlaceholder = .init(string: viewModel?.searchPlaceholder ?? "",
                                                                attributes: [.foregroundColor: UIColor.lightGray])
    }
    
    private func customiseNavBar() {
        navigationController?.view.tintColor = .gray
    }
    
    // MARK: - Selectable
    
    /// If the list is selectable, a DetailView will display representing the details of the model, further than that of the Cell
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard viewModel?.selectable == true else { return }
        
        // TODO: Move VC instantiation/VM injection to Coordinator, call view change in Coordinator via VM
        guard let detailStoryboardName = detailStoryboardName, let detailViewControllerID = detailViewControllerID else { return }
            
        let storyboard = UIStoryboard(name: detailStoryboardName, bundle: Bundle(for: Self.self))
        guard let details: DetailViewController<Model> = storyboard.instantiateViewController(withIdentifier: detailViewControllerID) as? DetailViewController<Model> else { return }
        
        guard let model = viewModel?.displayList[indexPath.row] as? Model else { return }
        details.initiateViewModel(with: model)
        
        self.navigationController?.pushViewController(details, animated: true)
    }
}
