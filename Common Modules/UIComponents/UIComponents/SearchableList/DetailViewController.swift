//
//  DetailViewController.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 19/06/2022.
//

import AppComponents
import UIKit

/// Display the details for a model
/// Works with SearchableListViewController as the VC displayed on selecting a list item
open class DetailViewController<Model: Codable>: UIViewController {
    /// Allows access to the model and other relevant logic or data for this view
    public var viewModel: DetailViewModel<Model>?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        populateView()
    }
    
    /// Sets the view model for this view controller
    /// Won't need to override unless need to access more than just the model
    /// E.g. Transform model data, additional functionality beyond displaying the model.
    open func initiateViewModel(with model: Model) {
        self.viewModel = DetailViewModel<Model>(model: model)
    }
    
    /// Sets the view components to display the model's values
    /// Can be overridden in a subclass to set custom values/components to that model/view
    open func populateView() {
        // Override in subclass to set model specific values
    }
}
