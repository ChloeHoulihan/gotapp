//
//  DetailViewModel.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 19/06/2022.
//

open class DetailViewModel<Model: Codable> {
    public let model: Model
    
    public init(model: Model) {
        self.model = model
    }
}
