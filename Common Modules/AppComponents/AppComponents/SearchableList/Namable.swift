//
//  Namable.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 18/06/2022.
//

public protocol Namable: Codable {
    var name: String { get }
    
    func isSearchMatch(to searchTerm: String) -> Bool
}

public extension Namable {
    /// Check's if this model is a match to a search string
    /// Can override to change what is matched
    func isSearchMatch(to searchTerm: String) -> Bool {
        return nameIsMatch(to: searchTerm)
    }
    
    /// Checks if this model's name is a match to a search string
    /// Can be used by isSearchMatch if overrode
    func nameIsMatch(to searchTerm: String) -> Bool {
        return isSubstring(substring: searchTerm, superstring: name)
    }
    
    /// Checks if a string (substring) is a substring of another (superstring) ignoring case
    func isSubstring(substring: String, superstring: String) -> Bool {
        return superstring.lowercased().contains(substring.lowercased())
    }
}
