//
//  ListDataProvider.swift
//  
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import Combine
import Foundation

public enum DataError: Error {
    // TODO: Fill in actual errors
    case failedToFetch
}

/// Handles fetching of data from endpoints and converting JSON data to defined models
open class ListDataProvider<Model: Namable> {
    let url: String
    let pageSize: Int
    
    var cancellables = Set<AnyCancellable>()
    
    public init(url: String, pageSize: Int) {
        self.url = url
        self.pageSize = pageSize
    }
    
    open func publisher(page: Int = 1) -> AnyPublisher<[Model], DataError> {
        Future<[Model], DataError> { [weak self] promise in
            
            // TODO: Full error checking/handling
            
            guard let self = self else {
                promise(.failure(.failedToFetch))
                return
            }
            
            guard let url = URL(string: "\(self.url)?page=\(page)&pageSize=\(self.pageSize)") else {
                return promise(.failure(.failedToFetch))
            }
            
            self.fetchData(url: url)
                .sink(receiveCompletion: { (completion) in
                    switch completion {
                    case .finished:
                        break
                    case .failure(let error):
                        print (error)
                    }
                }, receiveValue: { (response) in
                    promise(.success(response))
                })
                .store(in: &self.cancellables)
        }.eraseToAnyPublisher()
    }
    
    // TODO: Seperate out network logic into a RESTServices (eg) object so that it can be mocked and injected to test this class
    
    func fetchData(url: URL) -> AnyPublisher<[Model], Error> {
        URLSession.shared.dataTaskPublisher(for: url)
            .receive(on: DispatchQueue.main)
            .tryMap({ (data, response) -> Data in
                return data
            })
            .decode(type: [Model].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
