//
//  SearchableListViewModel.swift
//  
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import Combine
import Foundation

public protocol Searchable {
    var currentPage: Int { get }
    var displayList: [Namable] { get }
    var searchPlaceholder: String { get }
    var searchTerm: String { get set }
    var selectable: Bool { get }
    
    func getNextPage(completion: @escaping () -> Void)
}

// SearchableListVM methods cannot be put in a protocol extension because they use dataProvider which is generic and so can't be defined in the protocol

/// Provides the basal functionality of a SearchableList
open class SearchableListViewModel<Model: Namable>: Searchable {
    
    // MARK: - Public fields
    
    public let searchPlaceholder: String = "Search"
    
    /// The final list to be displayed
    public var displayList: [Namable] {
        displayListTyped
    }
    
    public var searchTerm: String = "" {
        didSet {
            if !searchTerm.isEmpty { filter() } // Only filter when filteredCache is not cache
        }
    }
    
    public private(set) var currentPage: Int = 0
    /// Whether the rows in the list should be able to be selected to display a detailed view
    public private(set) var selectable: Bool
    
    // MARK: - Private fields
    
    private let dataProvider: ListDataProvider<Model>
    
    private var cache: [Model] = []
    private var dataSubscription: AnyCancellable?
    
    private var displayListTyped: [Model] {
        searchTerm.isEmpty ? cache : filteredCache
    }
    
    private var filteredCache: [Model] = [] // Seperate to cache so that we can re-filter later
    
    // MARK: - Methods
    
    public init(url: String, selectable: Bool = false) {
        self.dataProvider = ListDataProvider<Model>(url: url, pageSize: 50)
        self.selectable = selectable
    }
    
    /// An init that allows direct injection of a data provider for testability
    public init(dataProvider: ListDataProvider<Model>, selectable: Bool = false) {
        self.dataProvider = dataProvider
        self.selectable = selectable
    }

    public func getNextPage(completion: @escaping () -> Void) {
        currentPage += 1
        getData(completion: completion)
    }
    
    private func getData(completion: @escaping () -> Void) {
        dataSubscription = dataProvider.publisher(page: self.currentPage)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    // TODO: Handle errors
                    print(error)
                    return
                case .finished:
                    return
                }
            }, receiveValue: { [weak self] response in
                guard let self = self else { return }
                
                self.cache.append(contentsOf: response)
                completion()
            })
    }
    
    /// Fitlers the list based on the search critera provided
    private func filter() {
        filteredCache = cache.filter({ $0.isSearchMatch(to: searchTerm) })
    }
}
