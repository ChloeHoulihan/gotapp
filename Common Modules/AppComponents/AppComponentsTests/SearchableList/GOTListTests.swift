//
//  ListDataProvider.swift.swift
//  GOTAppTests
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents
import XCTest
import Combine

struct MockModel: Namable {
    var name: String
}

class MockDataProvider: ListDataProvider<MockModel> {
    override func publisher(page: Int) -> AnyPublisher<[MockModel], DataError> {
        return Just([
            MockModel(name: "Test 1"),
            MockModel(name: "Test 2"),
            MockModel(name: "Test 3")
        ])
        .setFailureType(to: DataError.self)
        .eraseToAnyPublisher()
    }
}

class GOTListTests: XCTestCase {
    func testInit() {
        let vm = SearchableListViewModel<MockModel>(dataProvider: MockDataProvider(url: ""))
        
        XCTAssertTrue(vm.displayList.isEmpty)
        XCTAssertEqual(vm.currentPage, 0)
        XCTAssertTrue(vm.searchTerm.isEmpty)
    }
    
    func testDataProvider() {
        let vm = SearchableListViewModel<MockModel>(dataProvider: MockDataProvider(url: ""))
        
        let finished1 = expectation(description: "DataProvider will finish fetching first page")
        vm.getNextPage(completion: { finished1.fulfill() } )
        
        wait(for: [finished1], timeout: 0.5)
        XCTAssertEqual(vm.displayList.count, 3)
        XCTAssertEqual(vm.currentPage, 1)
        
        let finished2 = expectation(description: "DataProvider will finish fetching second page")
        vm.getNextPage(completion: { finished2.fulfill() } )
        
        wait(for: [finished2], timeout: 0.5)
        XCTAssertEqual(vm.displayList.count, 6)
        XCTAssertEqual(vm.currentPage, 2)
    }
    
    func testFilter() {
        let vm = SearchableListViewModel<MockModel>(dataProvider: MockDataProvider(url: ""))
        let finished = expectation(description: "DataProvider will finish fetching first page")
        
        vm.getNextPage(completion: { finished.fulfill() } )
        wait(for: [finished], timeout: 0.5)
        
        // Test searching for 1 applicable match:
        vm.searchTerm = "1"
        XCTAssertEqual(vm.displayList.count, 1)
        XCTAssertTrue(vm.displayList.contains(where: { $0.name == "Test 1" }))
        
        // Test searching for a match to all items:
        vm.searchTerm = "Test"
        XCTAssertEqual(vm.displayList.count, 3)
        
        // Test no search:
        vm.searchTerm = ""
        XCTAssertEqual(vm.displayList.count, 3)
    }
}
