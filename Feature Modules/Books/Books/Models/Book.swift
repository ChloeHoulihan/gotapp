//
//  Book.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents
import Foundation

public struct Book: Namable {
    public let name: String
    
    public let url: String
    public let isbn: String
    public let authors: [String]
    public let numberOfPages: Int
    public let publisher: String
    public let country: String
    public let mediaType: String
    public let released: String
    public let characters: [String]
    
    /// The released date in human readable format
    public var displayDate: String {
        convertToDisplayDate(releasedDate: released)
    }
    
    /// Converts a date from expected format "yyyy-MM-dd'T'hh:mm:ss" to "dd MMMM yyyy"
    /// - Parameter releasedDate: The date to convert in format "yyyy-MM-dd'T'hh:mm:ss".
    /// - Returns: The input date in format "dd MMMM yyyy" or an error message
    private func convertToDisplayDate(releasedDate: String) -> String {
        let dateFormatter = DateFormatter() // TODO: Avoid creating DateFormatter every call
        var displayDate = "Release date unknown"
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        let date = dateFormatter.date(from: releasedDate)
        
        if let date = date {
            dateFormatter.dateFormat = "dd MMMM yyyy"
            displayDate = dateFormatter.string(from: date)
        }
        
        return "\(displayDate)"
    }
}
