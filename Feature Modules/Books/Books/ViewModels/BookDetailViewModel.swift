//
//  BookDetailViewModel.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 19/06/2022.
//

import AppComponents

public class BookDetailViewModel: DetailViewModel<Book> {
    public var displayAuthors: String {
        if model.authors.isEmpty { return "" }
        
        var text = "By "
        for (index, author) in model.authors.enumerated() {
            var prefix = ""
            if index > 0 {
                prefix = index < model.authors.count - 1 ? ", " : " and "
            }
            text = "\(text)\(prefix)\(author)"
        }
        
        return text
    }
    
    public var displayPublished: String {
        let publisherEmpty = model.publisher.isEmpty
        let countryEmpty = model.country.isEmpty
        
        let prefix = publisherEmpty && countryEmpty ? "" : "Published"
        let publisher = publisherEmpty ? "" : " by \(model.publisher)"
        let country = countryEmpty ? "" : " in \(model.country)"
        
        return "\(prefix)\(publisher)\(country)"
    }
}
