//
//  BooksViewModel.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents
import Combine

public protocol BooksListDataProviderProtocol: ListDataProvider<Book> {
    func publisher(page: Int) -> AnyPublisher<[Book], DataError>
}

public class BooksListDataProvider: ListDataProvider<Book>, BooksListDataProviderProtocol {
    public init(pageSize: Int) {
        super.init(url: "https://anapioficeandfire.com/api/books", pageSize: pageSize)
    }
}

public class BooksViewModel: SearchableListViewModel<Book> {
    public init(dataProvider: BooksListDataProviderProtocol = BooksListDataProvider(pageSize: 50)) {
        super.init(dataProvider: dataProvider, selectable: true)
    }
}
