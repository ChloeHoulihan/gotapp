//
//  HousesViewModel.swift
//  
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents
import Combine

public protocol HousesListDataProviderProtocol: ListDataProvider<House> {
    func publisher(page: Int) -> AnyPublisher<[House], DataError>
}

public class HousesListDataProvider: ListDataProvider<House>, HousesListDataProviderProtocol {
    public init(pageSize: Int) {
        super.init(url: "https://anapioficeandfire.com/api/houses", pageSize: pageSize)
    }
}

public class HousesViewModel: SearchableListViewModel<House> {
    public init(dataProvider: HousesListDataProviderProtocol = HousesListDataProvider(pageSize: 50)) {
        super.init(dataProvider: dataProvider)
    }
}
