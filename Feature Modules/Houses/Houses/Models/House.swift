//
//  House.swift
//  
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents

public struct House: Namable {
    public let name: String
    
    public let url: String
    public let region: String
    public let coatOfArms: String
    public let words: String
    public let titles: [String]
    public let seats: [String]
    public let currentLord: String
    public let heir: String
    public let overlord: String
    public let founded: String
    public let founder: String
    public let diedOut: String
    public let ancestralWeapons: [String]
    public let cadetBranches: [String]
    public let swornMembers: [String]
    
    public var displayWords: String {
        words.isEmpty ? "" : "\"\(words)\""
    }
}
