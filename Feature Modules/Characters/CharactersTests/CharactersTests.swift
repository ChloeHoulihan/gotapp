//
//  CharactersTests.swift
//  CharactersTests
//
//  Created by Chloe Houlihan on 19/06/2022.
//

import XCTest

class CharactersTests: XCTestCase {
    func testFilterCharacter() {
        let characters: [Character] = [
            Character(name: "Test 1", url: "", gender: "", culture: "", born: "", died: "", aliases: [], father: "", mother: "", spouse: "", allegiances: [], books: [], povBooks: [], tvSeries: [], playedBy: []),
            Character(name: "", url: "", gender: "", culture: "", born: "", died: "", aliases: ["Test 2", "Test 3"], father: "", mother: "", spouse: "", allegiances: [], books: [], povBooks: [], tvSeries: [], playedBy: []),
            Character(name: "Test 3", url: "", gender: "", culture: "", born: "", died: "", aliases: [], father: "", mother: "", spouse: "", allegiances: [], books: [], povBooks: [], tvSeries: [], playedBy: []),
            Character(name: "Test 4", url: "", gender: "", culture: "", born: "", died: "", aliases: [], father: "", mother: "", spouse: "", allegiances: [], books: [], povBooks: [], tvSeries: [], playedBy: []),
            Character(name: "Test 4", url: "", gender: "", culture: "", born: "", died: "", aliases: [], father: "", mother: "", spouse: "", allegiances: [], books: [], povBooks: [], tvSeries: [], playedBy: [])
        ]
        
        let searchByTest = characters.filter({ $0.isSearchMatch(to: "Test") })
        XCTAssertEqual(searchByTest.count, 5, "Search across all works")
        
        let searchBy3 = characters.filter({ $0.isSearchMatch(to: "3") })
        XCTAssertEqual(searchBy3.count, 2, "Search across names and aliases works")
        
        let searchBy2 = characters.filter({ $0.isSearchMatch(to: "2") })
        XCTAssertEqual(searchBy2.count, 1, "Search across alises works")
        
        let searchBy4 = characters.filter({ $0.isSearchMatch(to: "4") })
        XCTAssertEqual(searchBy4.count, 2, "Search across names works")
    }
}
