//
//  CharactersViewModel.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents
import Combine

public protocol CharactersListDataProviderProtocol: ListDataProvider<Character> {
    func publisher(page: Int) -> AnyPublisher<[Character], DataError>
}

public class CharactersListDataProvider: ListDataProvider<Character>, CharactersListDataProviderProtocol {
    public init(pageSize: Int) {
        super.init(url: "https://anapioficeandfire.com/api/characters", pageSize: pageSize)
    }
}

public class CharactersViewModel: SearchableListViewModel<Character> {
    public let cultureSubtitle = "Culture:"
    public let bornSubtitle = "Born:"
    public let diedSubtitle = "Died:"
    public let seasonsSubtitle = "Seasons"
    
    public init(dataProvider: CharactersListDataProviderProtocol = CharactersListDataProvider(pageSize: 50)) {
        super.init(dataProvider: dataProvider)
    }
}
