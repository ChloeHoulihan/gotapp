//
//  Character.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 15/06/2022.
//

import AppComponents

public struct Character: Namable {
    public let name: String
    
    public let url: String
    public let gender: String
    public let culture: String
    public let born: String
    public let died: String
    public let aliases:  [String]
    public let father: String
    public let mother: String
    public let spouse: String
    public let allegiances: [String]
    public let books: [String]
    public let povBooks: [String]
    public let tvSeries: [String]
    public let playedBy: [String]
    
    public init(name: String,
         url: String,
         gender: String,
         culture: String,
         born: String,
         died: String,
         aliases: [String],
         father: String,
         mother: String,
         spouse: String,
         allegiances: [String],
         books: [String],
         povBooks: [String],
         tvSeries: [String],
         playedBy: [String]) {
        self.name = name
        self.url = url
        self.gender = gender
        self.culture = culture
        self.born = born
        self.died = died
        self.aliases = aliases
        self.father = father
        self.mother = mother
        self.spouse = spouse
        self.allegiances = allegiances
        self.books = books
        self.povBooks = povBooks
        self.tvSeries = tvSeries
        self.playedBy = playedBy
    }
    
    public var displayName: String {
        name.isEmpty ? (aliases.first ?? "Unnamed") : name
    }
    
    public var displayBorn: String {
        born.isEmpty ? "Unknown" : born
    }
    
    public var displayDied: String {
        died.isEmpty ? "-" : died
    }
    
    public var season: String {
        var seasons: String = ""
        
        for season in tvSeries {
            if season == "Season 1" {
                seasons.append("I ")
            } else if season == "Season 2" {
                seasons.append("II, ")
            } else if season == "Season 3" {
                seasons.append("III, ")
            } else if season == "Season 4" {
                seasons.append("IV, ")
            } else if season == "Season 5" {
                seasons.append("V, ")
            } else if season == "Season 6" {
                seasons.append("VI, ")
            }  else if season == "Season 7" {
                seasons.append("VII, ")
            } else if season == "Season 8" {
                seasons.append("VIII")
            }
        }
        
        return seasons
    }
    
    /// Matches based on name or alias
    public func isSearchMatch(to searchTerm: String) -> Bool {
        return nameIsMatch(to: searchTerm) || aliasIsMatch(to: searchTerm)
    }
    
    private func aliasIsMatch(to searchTerm: String) -> Bool {
        return aliases.contains(where: { isSubstring(substring: searchTerm, superstring: $0) })
    }
}
