//
//  CharactersViewController.swift
//  iOSTakeHomeChallenge
//
//  Created by James Malcolm on 09/03/2021.
//

import AppComponents
import Characters
import UIComponents
import UIKit

public class CharactersViewController: SearchableListViewController<Character, CharacterTableViewCell> {
    /// Allows access to the CharactersVC specific values/functionality
    private var charactersViewModel: CharactersViewModel?
    
    override public func viewDidLoad() {
        cellId = "CharacterTableViewCell"
        charactersViewModel = CharactersViewModel()
        viewModel = charactersViewModel
        
        navigationController?.navigationBar.barStyle = .black
        
        super.viewDidLoad()
    }
}

public class CharacterTableViewCell: UITableViewCell, ListTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cultureSubtitle: UILabel!
    @IBOutlet weak var cultureLabel: UILabel!
    @IBOutlet weak var bornSubtitle: UILabel!
    @IBOutlet weak var bornLabel: UILabel!
    @IBOutlet weak var diedSubtitle: UILabel!
    @IBOutlet weak var diedLabel: UILabel!
    @IBOutlet weak var seasonSubtitle: UILabel!
    @IBOutlet weak var seasonLabel: UILabel!
    
    public func setupWith(model: Namable, viewModel: Searchable?) {
        setupSelectable(viewModel: viewModel)
        guard let character = model as? Character else { return }
        guard let viewModel = viewModel as? CharactersViewModel else { return }
        setupWith(character: character, viewModel: viewModel)
    }
    
    public func setupWith(character: Character, viewModel: CharactersViewModel) {
        customiseAppearance()
        
        nameLabel.text = character.displayName
        cultureSubtitle.text = viewModel.cultureSubtitle
        cultureLabel.text = character.culture
        bornSubtitle.text = viewModel.bornSubtitle
        bornLabel.text = character.displayBorn
        diedSubtitle.text = viewModel.diedSubtitle
        diedLabel.text = character.displayDied
        seasonSubtitle.text = viewModel.seasonsSubtitle
        seasonLabel.text = character.season
    }
    
    private func customiseAppearance() {
        nameLabel.font = .systemFont(ofSize: 22.0)
        nameLabel.textColor = .white
        
        let subtitles: [UILabel] = [cultureSubtitle, bornSubtitle, diedSubtitle, seasonSubtitle]
        let values: [UILabel] = [cultureLabel, bornLabel, diedLabel, seasonLabel]
        let subtitleFont: UIFont = .systemFont(ofSize: 17.0)
        
        for subtitle in subtitles {
            subtitle.textColor = .white
            subtitle.font = subtitleFont
        }
        
        for value in values {
            value.textColor = .lightGray
            value.font = subtitleFont
        }
    }
}
