//
//  HousesViewController.swift
//  iOSTakeHomeChallenge
//
//  Created by James Malcolm on 09/03/2021.
//

import AppComponents
import Houses
import UIComponents
import UIKit

public class HousesViewController: SearchableListViewController<House, HouseTableViewCell> {
    /// Allows access to the HousesVC specific values/functionality
    private var housesViewModel: HousesViewModel?
    
    override public func viewDidLoad() {
        cellId = "HouseTableViewCell"
        housesViewModel = HousesViewModel()
        viewModel = housesViewModel
        
        navigationController?.navigationBar.barStyle = .black
        
        super.viewDidLoad()
    }
}

public class HouseTableViewCell: UITableViewCell, ListTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var wordsLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    
    
    public func setupWith(model: Namable, viewModel: Searchable?) {
        setupSelectable(viewModel: viewModel)
        guard let house = model as? House else { return }
        setupWith(house: house)
    }
    
    public func setupWith(house: House) {
        customiseAppearance()
        
        nameLabel.text = house.name
        wordsLabel.text =  house.displayWords
        regionLabel.text = house.region
    }
    
    private func customiseAppearance() {
        nameLabel.font = .systemFont(ofSize: 22.0)
        nameLabel.textColor = .white
        
        regionLabel.font = .systemFont(ofSize: 17.0)
        regionLabel.textColor = .lightGray
        
        wordsLabel.font = .systemFont(ofSize: 17.0)
        wordsLabel.textColor = .lightGray
    }
}
