//
//  BookDetailViewController.swift
//  GOTApp
//
//  Created by Chloe Houlihan on 19/06/2022.
//

import AppComponents
import Books
import UIComponents
import UIKit

public class BookDetailViewController: DetailViewController<Book> {
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var authorsLabel: UILabel!
    @IBOutlet public weak var releasedLabel: UILabel!
    @IBOutlet public weak var publishedLabel: UILabel!
    
    override public func initiateViewModel(with model: Book) {
        viewModel = BookDetailViewModel(model: model)
    }
    
    override public func populateView() {
        super.populateView()
        
        customiseAppearance()
        
        guard let model = viewModel?.model else { return }
        
        titleLabel.text = model.name
        releasedLabel.text = model.displayDate
        
        guard let viewModel = viewModel as? BookDetailViewModel else { return }
        authorsLabel.text = viewModel.displayAuthors
        publishedLabel.text = viewModel.displayPublished
    }
    
    // TODO: Finish design (in Storyboard also)
    private func customiseAppearance() {
        titleLabel.font = .systemFont(ofSize: 22.0)
        titleLabel.textColor = .white
        
        let subtitle: UIFont = .systemFont(ofSize: 17.0)
        
        authorsLabel.font = subtitle
        authorsLabel.textColor = .lightGray
        
        releasedLabel.font = subtitle
        releasedLabel.textColor = .lightGray
        
        publishedLabel.font = .systemFont(ofSize: 12.0)
        publishedLabel.textColor = .gray
    }
}
