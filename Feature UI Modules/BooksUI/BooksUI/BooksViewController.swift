//
//  BooksViewController.swift
//  iOSTakeHomeChallenge
//
//  Created by James Malcolm on 09/03/2021.
//

import AppComponents
import Books
import UIComponents
import UIKit

public class BooksViewController: SearchableListViewController<Book, BooksTableViewCell> {
    /// Allows access to the BookVC specific values/functionality
    private var booksViewModel: BooksViewModel?
    
    override public func viewDidLoad() {
        cellId = "BooksTableViewCell"
        detailStoryboardName = "Books"
        detailViewControllerID = "BookDetailView"
        
        booksViewModel = BooksViewModel()
        viewModel = booksViewModel
        
        navigationController?.navigationBar.barStyle = .black
        
        super.viewDidLoad()
    }
}

public class BooksTableViewCell: UITableViewCell, ListTableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pagesLabel: UILabel!
    
    public func setupWith(model: Namable, viewModel: Searchable?) {
        setupSelectable(viewModel: viewModel)
        guard let book = model as? Book else { return }
        setupWith(book: book)
    }
    
    public func setupWith(book: Book) {
        customiseAppearance()
        
        titleLabel.text = book.name
        dateLabel.text = book.displayDate
        pagesLabel.text =  "\(book.numberOfPages)"
    }
    
    private func customiseAppearance() {
        titleLabel.font = .systemFont(ofSize: 22.0)
        titleLabel.textColor = .white
        
        dateLabel.font = .systemFont(ofSize: 17.0)
        dateLabel.textColor = .lightGray
        
        pagesLabel.font = .systemFont(ofSize: 17.0)
        pagesLabel.textColor = .lightGray
    }
}
